<?php
require('config.php');
require('sql.php');
require('includes.php');

if($_GET['team']) {
  $team = getTeams($_GET['team'])[0];
} else {
  echo '<head><meta name="viewport" content="user-scalable=no, width=device-width"></head>';
  echo '<link rel="stylesheet" href="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.css" />
        <script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
        <script src="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.js"></script>';
  echo '<form data-ajax="false">';
  echo '<select name="team">';
  echo '<option></option>';
  foreach(getSchedule() as $n){
    if(!$n['team']){continue;}
    // $s = getSchedule(NULL, false, NULL, $n['id'])[0];
    echo '<option value="'.$n['team'].'">'.date("g:ia", strtotime($n['time'])) . ' - ' . getTeams($n['team'])[0]['name'] .'</option>';
  }
  echo '</select>';
  echo '<button type="submit">SELECT</button>';
  echo '</form>';
  die();
}
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="user-scalable=no, width=device-width">
    <link rel="stylesheet" href="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.css" />
    <script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
    <script src="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.js"></script>
    <link rel="stylesheet" type="text/css" href="style_team_admin.css">
    <title>Team <?php echo $team['id']; ?></title>
  </head>
  <body data-theme="b" data-role="page">
<?php
    //GET GETS & POSTS
    if($_POST['stopClock']) {
      if(!getRecords($_POST['stopClock'])[0]['stop']){
        update('records', $_POST['stopClock'], 'stop', date("Y-m-d H:i:s"));
      }
    }
    if($_POST['startClock']){
      newRecord($_POST['startClock'], date("Y-m-d H:i:s"));
    }
    if($_POST){
      update('records', $team['id'], 'clues', $_POST['clues'], TRUE);
      update('records', $team['id'], 'cheat', $_POST['cheat'], TRUE);
    }

    $record = getRecords($_GET['team'])[0];
    $schedule = getSchedule(NULL, false, NULL, $_GET['team'], NULL)[0];

    echo '<div class="header">#'.$team['id'].' - '.$team['name'].'</div>';
    echo '<div class="room">Room - '.$schedule['room'].'</div>';

    if($record['start'] && !$record['stop']) {
      echo '<div id="liveclock" class="clock">'.ltrim(date("i:s", (time() - strtotime($record['start']))), "0").'</div>';
      echo '<script>';
      echo 'var countDownDate = new Date("'.date("Y-m-d\TH:i:s\Z", strtotime($record['start'])).'").getTime();
            console.log(countDownDate)
            var x = setInterval(function() {
              var now = new Date().getTime();
              var distance = now - countDownDate;
              var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
              var seconds = Math.floor((distance % (1000 * 60)) / 1000);
              document.getElementById("liveclock").innerHTML = minutes + ":" + pad(seconds);
            }, 1000);';
      echo '</script>';
      $startBTNstatus = ' disabled ';
      $stopBTNstatus = '';
    } elseif($record['start'] && $record['stop']) {
      echo '<div id="deadclock" class="clock">'.ltrim(date("i:s", (strtotime($record['stop']) - strtotime($record['start']))), "0").'</div>';
      $startBTNstatus = ' disabled ';
      $stopBTNstatus = ' disabled ';
    } else {
      echo '<div class="clock">--:--</div>';
      $startBTNstatus = '';
      $stopBTNstatus = '';
    }
    echo '<form method="post" data-ajax="false" id="theform">';

    echo '<button type="submit" name="startClock" id="startClock" value="'.$team['id'].'" '.$startBTNstatus.'>start</button>';

    echo '<button type="submit" name="stopClock" id="stopClock" value="'.$team['id'].'" '.$stopBTNstatus.' onclick="return confirm(\'Are you sure you want to stop the clock?\');">stop</button>';

    echo '<div class="clues" data-theme="b">
            <label for="numOfClues">Clues Given:</label>
            <select id="numOfClues" name="clues" data-iconpos="right">';
              foreach(array(0,1,2,3,4,5) as $n){
                if($record['clues'] == $n){$sel = ' selected ';} else {$sel='';}
                echo "<option value='$n' $sel>$n</option>";
              }
          echo '</select>
          </div>';

    echo '<div class="cheat" data-theme="b">
            <label for="cheatBOX">Times Cheating:</label>
            <select id="cheatBOX" name="cheat" data-iconpos="right">';
              foreach(array(0,1,2,3,4,5) as $n){
                if($record['cheat'] == $n){$sel = ' selected ';} else {$sel='';}
                echo "<option value='$n' $sel>$n</option>";
              }
          echo '</select>
          </div>';

    // if($record['cheat']){$cheatCHK=' checked ';}else{$cheatCHK='';}
    // echo '<div class="cheat">
    //       <input id="cheatBOX" name="cheat" type="checkbox" value="1" '.$cheatCHK.'>
    //       <label for="cheatBOX">Cheating?</label>
    //       </div>';
    // echo '</form>'
    ?>
</body>
</html>
<script>
function pad(num) {
  var s = num+"";
  while (s.length < 2) s = "0" + s;
  return s;
}

$('#numOfClues').blur(function(){
  $('#theform').submit();
});

$('#cheatBOX').blur(function(){
  $('#theform').submit();
});
</script>
