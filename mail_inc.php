<?php
date_default_timezone_set('Etc/UTC');
require('PHPMailer-master/PHPMailerAutoload.php');

function congMail($to, $from = EMAIL_USE_SMTP, $subject, $payload, $attachment = NULL, $password){
  if(!$subject
  || !$to
  || !$payload){
    return false;
  }


  $X = getSMTPsettings($from);

  //Create a new PHPMailer instance
  $mail = new PHPMailer;

  //Tell PHPMailer to use SMTP
  $mail->isSMTP();

  //Enable SMTP debugging
  // 0 = off (for production use)
  // 1 = client messages
  // 2 = client and server messages
  $mail->SMTPDebug = 0;

  //Ask for HTML-friendly debug output
  $mail->Debugoutput = 'html';

  //Set the hostname of the mail server
  $mail->Host = $X['host'];
  // use
  // $mail->Host = gethostbyname('smtp.gmail.com');
  // if your network does not support SMTP over IPv6

  //Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
  $mail->Port = $X['port'];

  //Set the encryption system to use - ssl (deprecated) or tls
  $mail->SMTPSecure = $X['SMTPSecure'];

  //Whether to use SMTP authentication
  $mail->SMTPAuth = $X['SMTPAuth'];

  //Username to use for SMTP authentication - use full email address for gmail
  $mail->Username = $X['address'];

  //Password to use for SMTP authentication
  $mail->Password = $password;

  //Set who the message is to be sent from
  $mail->setFrom($X['address']);

  //Set an alternative reply-to address
  //$mail->addReplyTo($from, 'Jonathan Stanford');
  // $mail->addReplyTo($from);

  //Set who the message is to be sent to
  $mail->addAddress($to);

  //Set the subject line
  $mail->Subject = $subject;

  //Read an HTML message body from an external file, convert referenced images to embedded,
  //convert HTML into a basic plain-text alternative body
    // $mail->msgHTML(file_get_contents(output.php), dirname(__FILE__));
    $mail->msgHTML($payload);


  //Replace the plain text body with one created manually
  //$mail->AltBody = 'This is a plain-text message body';

  //Attach an image file
  if($attachment){
    $mail->addAttachment($attachment);
  }

  //send the message, check for errors
  if (!$mail->send()) {
      // echo "Mailer Error: " . $mail->ErrorInfo;
      return 0;
  } else {
      // echo "Message sent!";
      // transactionLog("congMail $to $subject");
      return 1;
  }
}



function getSMTPsettings($address){
  $domain = explode('@',$address)[1];

  switch ($domain) {
    case 'stanfordscomputer.com':
    case 'gmail.com':
      $host = 'smtp.gmail.com';
      $port = 587;
      $SMTPSecure = 'tls';
      $SMTPAuth = true;
      break;

    case 'yahoo.com':
      $host = 'smtp.mail.yahoo.com';
      $port = 587;
      $SMTPSecure = 'tls';
      $SMTPAuth = true;
      break;

    case 'outlook.com':
      $host = 'smtp-mail.outlook.com';
      $port = 587;
      $SMTPSecure = 'tls';
      $SMTPAuth = true;
      break;

    case 'live.com':
    case 'hotmail.com':
    case 'msn.com':
      $host = 'smtp.live.com';
      $port = 25;
      $SMTPSecure = 'tls';
      $SMTPAuth = true;
      break;

    case 'comcast.net':
      $host = 'smtp.comcast.net';
      $port = 587;
      $SMTPSecure = 'tls';
      $SMTPAuth = true;
      break;

    case 'verizon.net':
      $host = 'smtp.verizon.net';
      $port = 465;
      $SMTPSecure = 'ssl';
      $SMTPAuth = true;
      break;

    case 'icloud.com':
    case 'mac.com':
      $host = 'smtp.mail.me.com';
      $port = 587;
      $SMTPSecure = 'tls';
      $SMTPAuth = true;
      break;

    case 'juno.com':
      $host = 'smtp.juno.com';
      $port = 465;
      $SMTPSecure = 'ssl';
      $SMTPAuth = true;
      break;

    case 'frontier.com':
      $host = 'smtp.zoominternet.net';
      $port = 587;
      $SMTPSecure = 'tls';
      $SMTPAuth = true;
      break;

    case 'zoominternet.net':
      $host = 'smtp.frontier.com';
      $port = 465;
      $SMTPSecure = 'ssl';
      $SMTPAuth = true;
      break;

    case 'millsinfo.com':
      $host = 'mail.millsinfo.com';
      $port = 25;
      $SMTPSecure = false;
      $SMTPAuth = true;
      break;

    case 'ameritech.net':
    case 'att.net':
    case 'bellsouth.net':
    case 'flash.net':
    case 'nvbell.net':
    case 'pacbell.net':
    case 'prodigy.net':
    case 'sbcglobal.net':
    case 'snet.net':
    case 'swbell.net':
    case 'wans.net':
      $host = 'outbound.att.net';
      $port = 465;
      $SMTPSecure = 'ssl';
      $SMTPAuth = true;
      break;

    default:
      $host = NULL;
      $port = NULL;
      $SMTPSecure = NULL;
      $SMTPAuth = NULL;
      break;
  }

  return array('address'=>$address,'host'=>$host,'port'=>$port,'SMTPSecure'=>$SMTPSecure,'SMTPAuth'=>$SMTPAuth);
}

?>
