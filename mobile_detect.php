<?php
//Detect special conditions devices
$iPod    = stripos($_SERVER['HTTP_USER_AGENT'],"iPod");
$iPhone  = stripos($_SERVER['HTTP_USER_AGENT'],"iPhone");
$iPad    = stripos($_SERVER['HTTP_USER_AGENT'],"iPad");
$Android = stripos($_SERVER['HTTP_USER_AGENT'],"Android");
$webOS   = stripos($_SERVER['HTTP_USER_AGENT'],"webOS");

//do something with this information
if( $iPod || $iPhone || $iPad || $Android || $webOS ){
    echo '<div class="center box">';
    echo "<h2>This site is only viewable from a Bethel computer.</h2>";
    echo '<h4>http://'.$_SERVER['HTTP_HOST'].'</h4>';
    echo "</div>";
    die();
}
?>
