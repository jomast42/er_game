<?php
function getSchedule($ampm=NULL, $available_only=false, $id=NULL, $team=NULL, $time=NULL) {
	$table = 'schedule';
  if($ampm) {
    if($available_only){$w='AND `team` IS NULL';} else {$w='';}
    if($ampm == 'am'){ $result = mysqli_query($GLOBALS['con'],"SELECT * FROM $table WHERE `time`<'12:00:00' $w"); }
    if($ampm == 'pm'){ $result = mysqli_query($GLOBALS['con'],"SELECT * FROM $table WHERE `time`>='12:00:00' $w"); }
  } else {
    if($available_only){$w='WHERE `team` IS NULL';} else {$w='';}
    $result = mysqli_query($GLOBALS['con'],"SELECT * FROM $table $w");
  }
  if($id){$result = mysqli_query($GLOBALS['con'],"SELECT * FROM $table WHERE `id`=$id LIMIT 1");}
  if($team){$result = mysqli_query($GLOBALS['con'],"SELECT * FROM $table WHERE `team`=$team LIMIT 1");}
  if($time){$result = mysqli_query($GLOBALS['con'],"SELECT * FROM $table WHERE `time`='$time'");}
	$data = array(); while (true) { $temp = mysqli_fetch_array($result, MYSQLI_ASSOC); if ($temp == false) { break; } $data[] = $temp; }

	return $data;
}

function getRecords($team=NULL, $active=NULL) {
	$table = 'records';
  if($team) {
    $result = mysqli_query($GLOBALS['con'],"SELECT * FROM $table WHERE `team`=$team LIMIT 1");
  } else {
    $result = mysqli_query($GLOBALS['con'],"SELECT * FROM $table");
  }
	if($active){
		$result = mysqli_query($GLOBALS['con'],"SELECT * FROM $table
																						WHERE `start` IS NOT NULL
																						AND `stop` IS NULL");
	}
	$data = array(); while (true) { $temp = mysqli_fetch_array($result, MYSQLI_ASSOC); if ($temp == false) { break; } $data[] = $temp; }

	return $data;
}

function getTeams($id = NULL, $email = NULL) {
	$table = 'teams';
  if($id) {
    $result = mysqli_query($GLOBALS['con'],"SELECT * FROM $table WHERE `id`=$id LIMIT 1");
  } elseif($email) {
    $result = mysqli_query($GLOBALS['con'],"SELECT * FROM $table WHERE `email`='$email' LIMIT 1");
  } else {
    $result = mysqli_query($GLOBALS['con'],"SELECT * FROM $table");
  }
	$data = array(); while (true) { $temp = mysqli_fetch_array($result, MYSQLI_ASSOC); if ($temp == false) { break; } $data[] = $temp; }

	return $data;
}

function newSchedule($time) {
  $time = mysqli_real_escape_string($GLOBALS['con'],$time);
  $table = 'schedule';

  $sql = "INSERT INTO $table (`time`)
  VALUES ('$time')";

  if (mysqli_query($GLOBALS['con'], $sql)) {
      return mysqli_insert_id($GLOBALS['con']);
  } else {
      return false;
  }
}

function newRecord($team, $start) {
  $team = mysqli_real_escape_string($GLOBALS['con'],$team);
  $start = mysqli_real_escape_string($GLOBALS['con'],$start);
  $table = 'records';

  $sql = "INSERT INTO $table (`team`,`start`)
  VALUES ($team, '$start')";

  if (mysqli_query($GLOBALS['con'], $sql)) {
      return mysqli_insert_id($GLOBALS['con']);
  } else {
      return false;
  }
}

function newTeam($name) {
  $name = mysqli_real_escape_string($GLOBALS['con'],$name);
  $table = 'teams';

  $sql = "INSERT INTO $table (`name`)
  VALUES ('$name')";

  if (mysqli_query($GLOBALS['con'], $sql)) {
      return mysqli_insert_id($GLOBALS['con']);
  } else {
      return false;
  }
}

function deleteTeam($id) {
  $id = mysqli_real_escape_string($GLOBALS['con'],$id);
  $table = 'teams';

  $sql = "DELETE FROM `$table` WHERE `id`=$id";

  if (mysqli_query($GLOBALS['con'], $sql)) {
      return true;
  } else {
      return false;
  }
}

function update($table, $id, $key, $value, $force_int = false){
  $value = mysqli_real_escape_string($GLOBALS['con'],$value);
  switch ($key) {
    case 'finish':
    case 'failval':
    case 'sysfail':
    case 'wrongcmd':
      $value = "`$key` + $value";
      $fakeint = true;
      break;

    default:
      break;
  }

  if($table=='schedule' || $table=='teams') {
    if(is_int($value) || $fakeint || $force_int){
      $result = mysqli_query($GLOBALS['con'],"UPDATE `$table` SET `$key`=$value WHERE `id`=$id");
    } else {
      $result = mysqli_query($GLOBALS['con'],"UPDATE `$table` SET `$key`='$value' WHERE `id`=$id");
    }
  } else {
    if(is_int($value) || $fakeint || $force_int){
      $result = mysqli_query($GLOBALS['con'],"UPDATE `$table` SET `$key`=$value WHERE `team`=$id");
    } else {
      $result = mysqli_query($GLOBALS['con'],"UPDATE `$table` SET `$key`='$value' WHERE `team`=$id");
    }
  }
}

function getBoardData() {
  $result = mysqli_query($GLOBALS['con'],
                         "SELECT *, `teams`.`id` AS `team`, `schedule`.`id` AS `id`
                         FROM `schedule`
                         LEFT JOIN `teams` ON schedule.team=teams.id
                         LEFT JOIN `records` ON records.team=teams.id
                         ORDER BY `time`
                         ");

  $data = array(); while (true) { $temp = mysqli_fetch_array($result, MYSQLI_ASSOC); if ($temp == false) { break; } $data[] = $temp; }
	return $data;
}

function getLeaders() {
  $table = 'records';
  $max = $GLOBALS['max_time_allowed'];
  $result = mysqli_query($GLOBALS['con'],
                         "SELECT *, (TIME_TO_SEC(TIMEDIFF(`stop`,`start`)) + (`clues` * 60) + (`cheat` * 300)) AS `result`
                         FROM `$table`
                         WHERE `finish`=1
                           AND `sysfail`=0
                           AND `correctpass`=1
                           AND `correctval`=1
                           AND `stop` IS NOT NULL
                           AND TIME_TO_SEC(TIMEDIFF(`stop`,`start`)) < $max
                      ORDER BY `result`
                         ");
  $data = array(); while (true) { $temp = mysqli_fetch_array($result, MYSQLI_ASSOC); if ($temp == false) { break; } $data[] = $temp; }
  return $data;
}
 ?>
