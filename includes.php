<?php
function loginPrompt(){
  echo '<div class="center box loginbox">
        <form class="" method="post" autocomplete="off">
            <ul>
            <!-- azariah -->
            <!-- matthew -->
            <!-- mark -->
            <!-- luke -->
            <!-- john -->
          <!--<li><label for="er_username">Username:</label></li>
              <li><input type="text" id="er_username" name="er_username" value="" required></li>
              <li><label for="er_password">Password:</label></li>
              <li><input type="password" id="er_password" name="er_password" value="" required></li> -->
              <li><label for="er_team">Team #</label></li>
              <li><input type="text" id="er_team" name="er_team" value="'.$_POST['er_team'].'" required autofocus></li>
              <li><button type="submit" name="button">Login</button></li>
            </tr>
          </ul>
          <input type="hidden" name="source" value="login">
        </form>
      </div>';
}

function logo() {
  echo '<div class="logo center"></div>';
  echo $GLOBALS['messages'];
}

function vaultPrompt() {
  update('records', $GLOBALS['team'], 'correctpass', 1);
  echo '<!-- azariah -->
        <!-- matthew -->
        <!-- mark -->
        <!-- luke -->
        <!-- john -->
  ';
  echo '<div class="center box loginbox">
        <form class="" method="post" autocomplete="off">
            <ul>
              <li><label for="er_v1">Confirmation Code #1</label></li>
              <li><input type="text" id="er_v1" name="er_v1" value="" required autofocus></li>
              <li><label for="er_v2">Confirmation Code #2</label></li>
              <li><input type="text" id="er_v2" name="er_v2" value="" required></li>
              <li><label for="er_v3">Confirmation Code #3</label></li>
              <li><input type="text" id="er_v3" name="er_v3" value="" required></li>
              <li><label for="er_v4">Confirmation Code #4</label></li>
              <li><input type="text" id="er_v4" name="er_v4" value="" required></li>
              <li><button type="submit" name="button">Validate!</button></li>
            </tr>
          </ul>
          <input type="hidden" name="source" value="validate">
          <input type="hidden" name="er_team" value="'.$_POST['er_team'].'">
        </form>
        <div class="forgetwrapper"><a class="forget" href="#" onclick="getCodeHint();">Forget your codes?</a></div>
        <script>function getCodeHint() { alert("Checkmate concealing gastropods"); return false; }</script>
      </div>';
}

function commandPrompt() {
  update('records', $GLOBALS['team'], 'correctval', 1);
  $out = '';
  $buttons = array();
  $out .= '<div class="center box">
            <h3>Available Commands:</h3>';
  $buttons[] = '<form method="post">
              <button type="submit" name="er_cmd" value="1">Upload Data</button>
              <input type="hidden" name="source" value="command">
              <input type="hidden" name="er_team" value="'.$_POST['er_team'].'">
            </form>';
  $buttons[] = '<form method="post">
              <button type="submit" name="er_cmd" value="2">Download Data</button>
              <input type="hidden" name="source" value="command">
              <input type="hidden" name="er_team" value="'.$_POST['er_team'].'">
            </form>';
  $buttons[] = '<form method="post">
              <button type="submit" name="er_cmd" value="3">Migrate Data</button>
              <input type="hidden" name="source" value="command">
              <input type="hidden" name="er_team" value="'.$_POST['er_team'].'">
            </form>';
  $buttons[] = '<form method="post">
              <button type="submit" name="er_cmd" value="4">Compress Data</button>
              <input type="hidden" name="source" value="command">
              <input type="hidden" name="er_team" value="'.$_POST['er_team'].'">
            </form>';
  shuffle($buttons);
  foreach($buttons as $button) {
    $out .= $button;
  }
  $out .= '</div>';
  echo $out;
}

function checkAttempts(){
  switch (getRecords(NULL, false, NULL, $_POST['team'], NULL)[0]['failval']) {
    case 0:
      break;
    case 1:
      notice('2 Tries Remaining!');
      break;
    case 2:
      notice('1 Try Remaining!');
      break;
    case 3:
      notice('OK, OK, Last try!');
      break;
    default:
      fail();
      break;
  }
}

function warning($x) {
  $GLOBALS['messages'] = '<div class="center warning">'.$x.'</div>';
}

function notice($x) {
  $GLOBALS['messages'] = '<div class="center notice">'.$x.'</div>';
}

function win() {
  update('records', $GLOBALS['team'], 'stop', date("Y-m-d H:i:s"));
  update('records', $GLOBALS['team'], 'finish', 1);
  echo '<style>
          body  {background-color:black; color: limegreen;}
          #win  {margin:3em;  text-align:center; background-color:black;}
          #win2 {display: inline-block; text-align: left;}
          .line {display: none;}
        </style>';
  echo '<div id="win" class="center box">
          <div id="win2">
            <h2 id="x1" class="line">✓ Network Online</h2>
            <h2 id="x2" class="line">✓ Firewell Online</h2>
            <h2 id="x3" class="line">✓ Webserver Online</h2>
            <h2 id="x4" class="line">✓ Database Online</h2>
            <h2 id="x5" class="line">✓ Site Online</h2>
            <h3 id="fail">Executing...</h3>
            <form method="post">
            <h1 id="x6" class="line">SUCCESS!</h1>
            <h2 id="x7" class="line"><button type="submit">Click to Continue</button></h2>
            <input type="hidden" name="er_v1" value="'.$val_a.'">
            <input type="hidden" name="er_v2" value="'.$val_b.'">
            <input type="hidden" name="er_v3" value="'.$val_c.'">
            <input type="hidden" name="er_v4" value="'.$val_d.'">
            <input type="hidden" name="er_team" value="'.$_POST['er_team'].'">
            <input type="hidden" name="source" value="forDaWin">
            </form>
          </div>
        </div>';
  echo '<script language="JavaScript" type="text/javascript">
          setTimeout( function() { document.getElementById("x1").style.display="block"; }, 2500);
          setTimeout( function() { document.getElementById("x2").style.display="block"; }, 5000);
          setTimeout( function() { document.getElementById("x3").style.display="block"; }, 7500);
          setTimeout( function() { document.getElementById("x4").style.display="block"; }, 10000);
          setTimeout( function() { document.getElementById("x5").style.display="block"; }, 12500);
          setTimeout( function() { document.getElementById("fail").style.display="none"; }, 15000);
          setTimeout( function() { document.getElementById("x6").style.display="block"; }, 15000);
          setTimeout( function() { document.getElementById("x7").style.display="block"; }, 15000);
        </script>';
  die();
}

function penalty() {
  update('records', $GLOBALS['team'], 'wrongcmd', 1);
  echo '<style>
          body  {background-color:black; color: limegreen;}
          #penalty  {margin:3em;  text-align:center; background-color:black;}
          #penalty2 {display: inline-block; text-align: left;}
          .red {color: red;}
          .line {display: none;}
        </style>';
  echo '<div id="penalty" class="center box">
          <div id="penalty2">
            <h2 id="x1" class="line ">✓ Network Online</h2>
            <h2 id="x2" class="line ">✓ Firewell Online</h2>
            <h2 id="x3" class="line ">✓ Webserver Online</h2>
            <h2 id="x4" class="line red">X Database Offline</h2>
            <h2 id="x5" class="line red">X Site Offline</h2>
            <h3 id="fail">Executing...</h3>
            <form method="post">
            <button id="backbutton" class="line">Click Here To Go Back</button>
            <input type="hidden" name="er_v1" value="'.$GLOBALS['val_a'].'">
            <input type="hidden" name="er_v2" value="'.$GLOBALS['val_b'].'">
            <input type="hidden" name="er_v3" value="'.$GLOBALS['val_c'].'">
            <input type="hidden" name="er_v4" value="'.$GLOBALS['val_d'].'">
            <input type="hidden" name="source" value="validate">
            <input type="hidden" name="er_team" value="'.$_POST['er_team'].'">
            </form>
          </div>
        </div>';
  echo '<script language="JavaScript" type="text/javascript">
          setTimeout( function() { document.getElementById("x1").style.display="block"; }, 2500);
          setTimeout( function() { document.getElementById("x2").style.display="block"; }, 5000);
          setTimeout( function() { document.getElementById("x3").style.display="block"; }, 7500);
          setTimeout( function() { document.getElementById("x4").style.display="block"; }, 10000);
          setTimeout( function() { document.getElementById("x5").style.display="block"; }, 12500);
          setTimeout( function() { document.getElementById("fail").style.display="none"; }, 15000);
          setTimeout( function() { document.getElementById("backbutton").style.display="block"; }, 15000);
        </script>';
  die();
}

function fail() {
  update('records', $GLOBALS['team'], 'stop', date("Y-m-d H:i:s"));
  update('records', $GLOBALS['team'], 'sysfail', 1);
  echo '<style>
          body  {background-color:black;}
          #fail {margin:3em;  text-align:center;}
        </style>';
  echo '<div id="fail"><img src="images/systemfailure.gif"></div>';
  echo '<form method="post">';
  echo '<h2><button type="submit">Click to Continue</button></h2>';
  echo '<input type="hidden" name="source" value="SysFail">';
  echo '<input type="hidden" name="er_team" value="'.$_POST['er_team'].'">';
  echo '</form';
  die();
}

function egg() {
  update('records', $GLOBALS['team'], 'egg', 1);
  echo '<style>
          #egg {margin:3em; text-align:center;}
        </style>';
  echo '<div id="egg" class="center">
          <img src="images/picard.gif">
          <div class="center box"><h2>Nice Try!</h2></div>>
        </div>';
  die();
}

function debug($x) {
  echo '<pre style="background-color:lightblue; font-family:monospace;">';
  print_r($x);
  echo '</pre>';
}

function registration_form(){
  if(getSchedule(NULL, TRUE)){
    echo '
      <h2>Registration Form</h2>
      <form method="post">
        <label for="team_name">Team Name</label><span class="notes">(Need help coming up with a name? <span id="gen">Click Here</span>)</span><br>
        <input type="text" name="team_name" id="team_name" required value="'.$_POST['team_name'].'"><br>
        <label for="email">Contact Email</label><span class="notes">(Personal email addresses only!)</span><br>
        <input type="text" name="email" id="email" required value="'.$_POST['email'].'"><br>
        <label for="mem1">Member #1</label><span class="notes">(Required)</span><br>
        <input type="text" name="mem1" id="mem1" required value="'.$_POST['mem1'].'"><br>
        <label for="mem2">Member #2</label><span class="notes">(Required)</span><br>
        <input type="text" name="mem2" id="mem2" required value="'.$_POST['mem2'].'"><br>
        <label for="mem3">Member #3</label><span class="notes">(Required)</span><br>
        <input type="text" name="mem3" id="mem3" required value="'.$_POST['mem3'].'"><br>
        <label for="mem4">Member #4</label><span class="notes">(Required)</span><br>
        <input type="text" name="mem4" id="mem4" required value="'.$_POST['mem4'].'"><br>
        <label for="mem5">Member #5</label><span class="notes">(Optional)</span><br>
        <input type="text" name="mem5" id="mem5" value="'.$_POST['mem5'].'"><br>
        <label for="mem6">Member #6</label><span class="notes">(Optional)</span><br>
        <input type="text" name="mem6" id="mem6" value="'.$_POST['mem6'].'"><br>
        <label for="notes">Notes:</label><br>
        <textarea name="notes" id="notes" placeholder="Add any addional notes here..."></textarea><br>

        <label for="time">Pick a Time</label><br>
        <select name="time" id="time" required>
        <option></option>
        <option value="any">Any</option>';

        if(getSchedule('am', TRUE)){echo '<option value="am">Any AM</options>';}
        if(getSchedule('pm', TRUE)){echo '<option value="pm">Any PM</options>';}

        $schedule = getSchedule();
        foreach($schedule as $s) {
          if($s['team']){$d = 'disabled';} else {$d = '';}
          echo '<option value="'.$s['id'].'" '.$d.'>'.date("g:ia", strtotime($s['time'])).'</option>';
        }

        echo '</select>
        <br>
        <button type="submit">Click to Register</button>
      </form>

      <script>
      var adjectives = ["Attractive", "Bald", "Beautiful", "Chubby", "Clean", "Dazzling", "Drab", "Elegant", "Fancy", "Fit", "Flabby", "Glamorous", "Gorgeous", "Handsome", "Icy", "Juicy", "Kicking", "Long", "Magnificent", "Muscular", "Nice", "Plain", "Plump", "Quaint", "Rightous", "Scruffy", "Shapely", "Short", "Skinny", "Stocky", "Ashy", "Black", "Blue", "Gray", "Green", "Icy", "Lemon", "Mango", "Orange", "Purple", "Red", "Salmon", "White", "Yellow", "Better", "Careful", "Clever", "Dead", "Easy", "Famous", "Gifted", "Helpful", "Important", "Inexpensive", "Mushy", "Odd", "Poor", "Powerful", "Rich", "Shy", "Tender", "Vast", "Wrong", "Aggressive", "Agreeable", "Ambitious", "Brave", "Calm", "Delightful", "Eager", "Faithful", "Gentle", "Happy", "Jolly", "Kind", "Lively", "Nice", "Obedient", "Polite", "Proud", "Silly", "Thankful", "Victorious", "Witty", "Wonderful", "Zealous", "Big", "Colossal", "Fat", "Gigantic", "Great", "Huge", "Immense", "Large", "Little", "Mammoth", "Massive", "Microscopic", "Miniature", "Petite", "Puny", "Scrawny", "Short", "Small", "Tall", "Teeny", "Tiny", "Crashing", "Deafening", "Echoing", "Faint", "Harsh", "Hissing", "Howling", "Loud", "Melodic", "Noisy", "Purring", "Quiet", "Rapping", "Raspy", "Rhythmic", "Screeching", "Shrilling", "Squeaking", "Thundering", "Tinkling", "Wailing", "Whining", "Whispering", "Ancient", "Brief", "Early", "Fast", "Future", "Late", "Long", "Modern", "Old", "Old-fashioned", "Prehistoric", "Quick", "Rapid", "Short", "Slow", "Swift", "Young"]

      var nouns = ["Aardvarks", "Albatrosses", "Alligators", "Anteaters", "Antelopes", "Ants", "Armadillos", "Baboons", "Badgers", "Barnacles", "Barracudas", "Bats", "Bats", "Beagles", "Bears", "Beavers", "Bees", "Beetles", "Birds", "Bison", "Bloodhounds", "Boara", "Bobcats", "Buffalos", "Bulldogs", "Bullfrogs", "Butterflies", "Camels", "Caterpillars", "Catfish", "Cats", "Centipedes", "Chameleon", "Cheetahs", "Chickens", "Chimpanzees", "Chinchillas", "Chipmunks", "Clams", "Collies", "Corgis", "Cows", "Crabs", "Cranes", "Crocodiles", "Dachshunds", "Dalmatians", "Deer", "Dodos", "Dogs", "Dogs", "Dolphins", "Ducks", "Eagles", "Eels", "Elephant", "Elephants", "Falcons", "Ferrets", "Fish", "Flamingos", "Flounders", "Foxes", "Frogs", "Geckos", "Geese", "Gerbils", "Gibbons", "Giraffes", "Goats", "Gophers", "Gorilla", "Gorillas", "Grasshoppers", "Greyhounds", "Hamsters", "Hares", "Hippopotamuses", "Hornets", "Horses", "Hounds", "Humans", "Hummingbirds", "Hyenas", "Iguanas", "Impalas", "Jackals", "Jaguars", "Jellyfish", "Kangaroos", "Koalas", "Labradoodles", "Ladybirds", "Lemmings", "Lemurs", "Lemurs", "Leopards", "Ligers", "Lions", "Lizards", "Lizards", "Llamas", "Lobsters", "Macaws", "Magpies", "Mammoths", "Manatees", "Meerkats", "Mice", "Millipedes", "Mongeese", "Monkeys", "Monkies", "Mothes", "Mules", "Newts", "Opossums", "Ostriches", "Otters", "Owls", "Oysters", "Panthers", "Parrots", "Peacocks", "Pelicans", "Penguins", "Penguins", "Pheasants", "Pigs", "Piranhas", "Poodles", "Porcupines", "Possums", "Prawns", "Puffins", "Pugs", "Pumas", "Quails", "Rabbits", "Raccoons", "Rats", "Rattlesnakes", "Reindeer", "Rhinos", "Rhinos", "Robins", "Rottweilers", "Salamanders", "Salamanders", "Scorpions", "Seahorses", "Seals", "Seals", "Sharks", "Sheep", "Shepherds", "Shrimp", "Skunks", "Sloths", "Snails", "Snakes", "Spaniels", "Sparrows", "Spiders", "Sponges", "Squids", "Squirrels", "Squirrels", "Stingrays", "Swans", "Termites", "Terriers", "Tigers", "Toads", "Tortoises", "Tortoises", "Toucans", "Turkies", "Vultures", "Walruses", "Warthogs", "Wasps", "Weasels", "Wolverines", "Wolves", "Wombats", "Woodpeckers", "Yaks", "Yorkies", "Zebras"]


      $("#gen").click(function() {
          var first = randomEl(adjectives)
          var last = ""

          counter=0
          while(last.charAt(0) != first.charAt(0)){
            last = randomEl(nouns)
            counter++
            if(counter > 300) {break;} // for loop safety
          }

          $("#team_name").val(first+" "+last);
      });

      function randomEl(list) {
          var i = Math.floor(Math.random() * list.length);
          return list[i];
      }

      </script>';
  } else {
    echo '<br><br>';
    echo '<h2>Sorry, but all available slots are full.</h2><br>';
    echo '<h3>However, there may be a team that has room for one or two more...</h3>';
    echo '<h3><a href="http://board.jw-dc.org/?single=1">Check out the schedule</a>, find a team that does not have 6 members, and contact that team. If they agree to let you join, have them let us know.</h3>';
  }
}

function registration_conf() {
  echo '<h2>Please review your submition:</h2>';
  echo '<table id="reg_review">';
  echo '<tr><td>Team name:</td><td>' . $_POST['team_name'] . '</td></tr>';
  echo '<tr><td>Contact Email:</td><td>' . $_POST['email'] . '</td></tr>';
  echo '<tr><td>Member #1:</td><td>' . $_POST['mem1'] . '</td></tr>';
  echo '<tr><td>Member #2:</td><td>' . $_POST['mem2'] . '</td></tr>';
  echo '<tr><td>Member #3:</td><td>' . $_POST['mem3'] . '</td></tr>';
  echo '<tr><td>Member #4:</td><td>' . $_POST['mem4'] . '</td></tr>';
  if($_POST['mem5']) {echo '<tr><td>Member #5:</td><td>' . $_POST['mem5'] . '</td></tr>';}
  if($_POST['mem6']) {echo '<tr><td>Member #6:</td><td>' . $_POST['mem6'] . '</td></tr>';}

  switch ($_POST['time']) {
    case 'any':
      $time = 'Any Time';
      break;

    case 'am':
      $time = 'Any AM';
      break;

    case 'pm':
      $time = 'Any PM';
      break;

    default:
      $time = date("g:ia", strtotime(getSchedule(NULL, FALSE, $_POST['time'])[0]['time']));
      break;
  }
  echo '<tr><td>Time:</td><td>' . $time . '</td></tr>';
  echo '<tr><td>Notes:</td><td>' . $_POST['notes'] . '</td></tr>';
  echo '</table>';

  echo '<form method="post">
          <input type="hidden" name="team_name" value="'.$_POST['team_name'].'">
          <input type="hidden" name="email" value="'.$_POST['email'].'">
          <input type="hidden" name="mem1" value="'.$_POST['mem1'].'">
          <input type="hidden" name="mem2" value="'.$_POST['mem2'].'">
          <input type="hidden" name="mem3" value="'.$_POST['mem3'].'">
          <input type="hidden" name="mem4" value="'.$_POST['mem4'].'">
          <input type="hidden" name="mem5" value="'.$_POST['mem5'].'">
          <input type="hidden" name="mem6" value="'.$_POST['mem6'].'">
          <input type="hidden" name="time" value="'.$_POST['time'].'">
          <input type="hidden" name="notes" value="'.$_POST['notes'].'">
          <input type="hidden" name="confirm" value="1">
          <button type="submit" name="button">Confirm</button>
        </form>';
}

function registration_submit(){
  if($_POST['time'] == 'any') {
    $_POST['time'] = randomScheduleSelect()['id'];
  } elseif($_POST['time'] == 'am') {
    $_POST['time'] = randomScheduleSelect('am')['id'];
  } elseif($_POST['time'] == 'pm') {
    $_POST['time'] = randomScheduleSelect('pm')['id'];
  }
  if(getSchedule(NULL, FALSE, $_POST['time'])['team']){
    echo 'Sorry, someone must have swooped in and took the time slot you wanted, Try again!';
    registration_form();
  } elseif(!checkIfEmailIsAllowable($_POST['email'])) {
    echo '<h2>Sorry, It appears that you\'ve already registered.<br>
          If you feel that you\'ve reached this message in error,<br>
          please contact us at <a href="fkl.missionimplausible@gmail.com">FKL.MissionImplausible@gmail.com</a></h2>';
  } else {
    regSchedule();
    echo '<br><h2>You\'re all set!<br>You will receive an email confirming this appointment and a calendar reminder.</h2>';
    require('mail_inc.php');
    $tname = $_POST['team_name'];
    $mem1 = $_POST['mem1'];
    $mem2 = $_POST['mem2'];
    $mem3 = $_POST['mem3'];
    $mem4 = $_POST['mem4'];
    $mem5 = $_POST['mem5'];
    $mem6 = $_POST['mem6'];
    $email = $_POST['email'];
    $notes = $_POST['notes'];
    $time = date("g:ia", strtotime(getSchedule(NULL, FALSE, $_POST['time'])[0]['time']));

    $subject = 'Mission:Implausible Registration';
    $payload = "
    <h2>Here is your registration info:</h2>
    <table id='reg_review'>
    <tr><td>Team name:</td><td>$tname</td></tr>
    <tr><td>Contact Email:</td><td>$email</td></tr>
    <tr><td>Member #1:</td><td>$mem1</td></tr>
    <tr><td>Member #2:</td><td>$mem2</td></tr>
    <tr><td>Member #3:</td><td>$mem3</td></tr>
    <tr><td>Member #4:</td><td>$mem4</td></tr>
    ";
    if($_POST['mem5']) {$payload .= "<tr><td>Member #5:</td><td>$mem5</td></tr>";}
    if($_POST['mem6']) {$payload .= "<tr><td>Member #6:</td><td>$mem6</td></tr>";}
    if($_POST['notes']) {$payload .= "<tr><td>Notes:</td><td>$notes</td></tr>";}
    $payload .= "<tr><td>Time:</td><td><b>$time</b></td></tr>";
    $payload .= '</table>';
    $payload .= "<br>Please have the whole team at RCA-3016 by $time or sooner.<br>If you need to contact us, just reply to this email.";
    $payload .= "<br>Due to the tight schedule for the day, teams that are late will forfeit their turn.";
    $payload .= "<br><br>Thanks for participating!<br>-<i>The Mission IMPLAUSIBLE Team</i>";
    $attachment = prepPtIcs(getSchedule(NULL, FALSE, $_POST['time'])[0]['time']);

    $my_file = 'tmp/'.preg_replace('/[^0-9a-z]/i', '_', $tname).'_reminder.ics';
    $handle = fopen($my_file, 'w') or die('Cannot open file:  '.$my_file); //implicitly creates file
    fwrite($handle, $attachment);
    fclose($handle);

    congMail($_POST['email'], EMAIL_USE_SMTP, $subject, $payload, $my_file, EMAIL_SMTP_PASSWORD);
    unset($my_file);
  }
}

function regSchedule(){
  $tid = newTeam($_POST['team_name']);
  update('schedule', $_POST['time'], 'team', $tid);
  update('teams', $tid, 'email', $_POST['email']);
  update('teams', $tid, 'mem1', $_POST['mem1']);
  update('teams', $tid, 'mem2', $_POST['mem2']);
  update('teams', $tid, 'mem3', $_POST['mem3']);
  update('teams', $tid, 'mem4', $_POST['mem4']);
  if($_POST['mem5']){update('teams', $tid, 'mem5', $_POST['mem5']);}
  if($_POST['mem6']){update('teams', $tid, 'mem6', $_POST['mem6']);}
  if($_POST['notes']){update('teams', $tid, 'notes', $_POST['notes']);}
}

function randomScheduleSelect($ampm=NULL){
  $schedules = getSchedule($ampm, TRUE);
  shuffle($schedules);
  return $schedules[0];
}

function checkIfEmailIsAllowable($email){
  $t = getTeams(NULL, $email);
  if($t){
    return false;
  } else {
    return true;
  }
}

function prepPtIcs($time) {
  include_once 'ICS.php';

  $date = '2018-10-20';
  $dlines = array(); // leave this array empty
  $description = ''; // Leave this string empty

  $location = 'RCA-3016';
  $summery = "Mission IMPLAUSIBLE";
  $dlines[] = "Fishkill Family Day 2018";
  $dlines[] = "Mission IMPLAUSIBLE - Save the Data";
  $dlines[] = "FKL.MissionMmplausible@gmail.com";

  foreach($dlines as $d) {
    $description .= $d . '\\n';
  }

  $ics = new ICS(array(
    'dtstart' => "$date $time",
    'dtend' => "$date $time +30 minutes",
    'alarm' => '30M',
    'location' => $location,
    'summary' => $summery,
    'description' => $description
  ));
  return $ics->to_string();
}

function winvid(){
  echo '<div style="text-align:center;"><video width="100%" height="100%" autoplay><source src="images/Success2.mp4"></video></div>';
}

function failvid(){
  echo '<div style="text-align:center;"><video width="100%" height="100%" autoplay><source src="images/Failure2.mp4"></video></div>';
}

function findTeam($room){
	$records = getRecords(NULL, TRUE);
	foreach($records as $r){
		$s = getSchedule(NULL, false, NULL, $r['team'], NULL)[0];
		if(mb_strtoupper($s['room']) == mb_strtoupper($room)){
			return $s['team'];
		}
	}
}
 ?>
