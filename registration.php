<?php
require('config.php');
require('sql.php');
require('includes.php');
?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="user-scalable=no, width=device-width">
    <link href="https://fonts.googleapis.com/css?family=Luckiest+Guy" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <title>Mission:Implausible Registration</title>
    <style>
      #gen {
        display: inline-block;
        color: white;
        background-color: black;
        padding: 5px 8px;
        border-radius: 15px;
      }

      #reg_review tr td:first-child {
        text-align: right;
      }

      #reg_review tr td:nth-child(2) {
        font-weight: bold;
      }

      body {
        background-color: #black;
        background-image: url('images/FFD.png'), url('images/confidential.jpg');
        background-position: bottom right, center;
        background-attachment: fixed, scroll;
        background-size: 400px 400px, auto;
        background-repeat: no-repeat;
        padding-left: 0;
        padding-right: 0;
        padding-top: 153px;
        padding-bottom: 40px;
        margin: 0;
      }

      #header {
        background-color: black;
        color: white;
        display: block;
        text-align: center;
        margin: 0;
        padding: 20px;
        position: absolute;
        top: 0;
        left: 0;
        right: 0;
        text-align:
        overflow: visible;
      }

      @keyframes wobble {
        0% {transform: rotate(0deg);}
        36% {transform: rotate(0deg);}
        37% {transform: rotate(10deg);}
        38% {transform: rotate(-10deg);}
        39% {transform: rotate(10deg);}
        40% {transform: rotate(0deg);}
        100% {transform: rotate(0deg);}
      }

      #title {
        font-family: 'Courier New', Courier;
        font-size: 4em;
        display: inline-block;
      }

      #title2 {
        font-family: 'Luckiest Guy';
        font-size: 3em;
        display: inline-block;
        animation-name: wobble;
        animation-duration: 9s;
        animation-iteration-count: infinite;
      }

      #subtitle {
        font-family: 'Courier New', Courier;
        font-size: 2em;
      }

      form {
        margin-left: 1em;
      }

      label {
        font-size: 1.1em;
        font-weight: bold;
        margin-right: .5em;
        background-color: white;
      }

      input {
        margin-bottom: .7em;
        font-size: 1.1em;
        border: 1px dashed black;
      }

      .notes {
        font-family: verdana;
        font-size: .75em;
        font-style: italic;
        background-color: white;
      }

      button {
        background-color: black;
        color: white;
        padding: 5px 8px;
        border-radius: 15px;
        border: 1px solid white;
        font-size: 1.5em;
        font-weight: bold;
        font-family: verdana;
      }

      input, #notes {
        width: 210px;
        border: 1px dashed black;
      }

      #notes {
        height: 52px;
      }

      #time {
        background-color: black;
        color: white;
        font-size: 1.2em;
        margin: 0 0 1em 0;
        border: 1px dashed white;
        width: 210px;
      }

      .box {
        background-color: rgba(255, 255, 255, 0.8);
        display: inline-block;
        padding-left: .5em;
      }

      #contact {
        text-align: right;
        margin-top: -5px;
        margin-bottom: -10px;
        font-family: verdana, sans-serif;
      }

      #contact a {
        color: white;
        text-decoration: none;
      }
    </style>
  </head>
  <body>
    <div id="header">
      <div id="title">MISSION:</div><div id="title2"> IMPLAUSIBLE</div><br>
      <div id="subtitle">Save The Data</div><div id="contact"><a href="mailto:fkl.missionimplausible@gmail.com">FKL.MissionImplausible@gmail.com</a></div>
    </div>
    <div class="box">
    <?php
    if($_POST){
      if(!$_POST['confirm']) {
        registration_conf();
      } else {
        registration_submit();
      }
    } else {
      registration_form();
    }
    ?>
    </div>

    <script>
    $(document).ready(function(){
      var h = $('#header').css("height").replace('px','')
      h = +h +40
      $('body').css("padding-top", h + 'px')
    });
    </script>
</body>
</html>
