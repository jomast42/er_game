<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="style.css">
    <meta name="viewport" content="user-scalable=no, width=device-width">
    <title>JW.ORG ADMIN</title>
  </head>
  <body>
    <?php
    require('config.php');
    require('sql.php');
    require('mobile_detect.php');
    require('includes.php');

    $room = mb_strtoupper($_GET['room']);
    if($_POST['er_team']){
      $team = $_POST['er_team'];
    } else {
      $team = findTeam($room);
      $_POST['er_team'] = $team;
    }

    if(!$team){
      echo '<div class="center box">If you are reading this, something has gone terribly wrong! Contact the operator immediately!</div>';
      die();
    }

    // PROCESS INPUT
    switch ($_POST['source']) {
      // case 'login':
      //   if($_POST['er_team'] == $admin_reset_pin) {
      //     $_SESSION['invalid_count'] = 0;
      //     unset($_POST['er_team']);
      //     $stage = 1;
      //   } elseif($_POST['er_team']) {
      //     $s = getSchedule(NULL, false, NULL, $_POST['er_team']);
      //     $r = getRecords($team = $s[0]['team']);
      //     if($s[0]['team']
      //     && $r[0]['start']
      //     && !$r[0]['stop']){
      //       $stage = 1;
      //     } else {
      //       warning('Invalid ID! Double check your ID and try again.');
      //     }
      //   } else {
      //     warning('Error!');
      //       $stage = 1;
      //   }

        // if(mb_strtolower($_POST['er_username']) == $user
        // && $_POST['er_password'] == $pass
        // && $_POST['er_team']) {
        //   $stage = 1;
        // } elseif(mb_strtolower($_POST['er_username']) == 'adminreset'
        // && $_POST['er_password'] == 'adminreset') {
        //   $_SESSION['invalid_count'] = 0;
        //   unset($_POST['er_team']);
        //   $stage = 0;
        // } elseif(mb_strtolower($_POST['er_username']) == 'white'
        // && $_POST['er_password'] == 'gamblepig') {
        //   cheat();
        //   die();
        // } else{
        //   warning('Error!');
        //   $stage = 0;
        // }
        // break;

      case 'validate':
        if(mb_strtolower($_POST['er_v1']) == 'matthew'
        && mb_strtolower($_POST['er_v2']) == 'mark'
        && mb_strtolower($_POST['er_v3']) == 'luke'
        && mb_strtolower($_POST['er_v4']) == 'john') {
          egg();
          die();
        }

        if(mb_strtolower($_POST['er_v1']) == $val_a
        && mb_strtolower($_POST['er_v2']) == $val_b
        && mb_strtolower($_POST['er_v3']) == $val_c
        && mb_strtolower($_POST['er_v4']) == $val_d) {
          $stage = 2;
        } else {
          $stage = 1;
          update('records', $GLOBALS['team'], 'failval', 1);
        }
        break;

      case 'command':
        if($_POST['er_cmd'] == 3) {
          win();
        } else {
          penalty();
        }
        break;

      case 'forDaWin':
        $stage = 100;
        break;

      case 'SysFail':
        $stage = 101;
        break;

      default:
        $stage = 1;
        break;
    }


    // OUTPUT PROCCESSING
    switch ($stage) {
      case 2:
        logo();
        commandPrompt();
        break;

      case 100:
        winvid();
        break;

      case 101:
        failvid();
        break;

      case 1:
      default:
        checkAttempts();
        logo();
        vaultPrompt();
        break;
    }
    ?>
  </body>
</html>
