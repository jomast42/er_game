<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="style_board.css">
    <meta name="viewport" content="user-scalable=no, width=device-width">
    <meta http-equiv="refresh" content="10">
    <title>Mission:Implausible Board</title>
  </head>
  <body>
    <?php
    require('config.php');
    require('sql.php');
    require('includes.php');

    $bd = getBoardData();
    $bd_count = count($bd);

    // BUILD SCHEDULE DATA
    $schedule = array();
    foreach($bd as $s) {
      $members = array();
      foreach(array($s['mem1'],$s['mem2'],$s['mem3'],$s['mem4'],$s['mem5'],$s['mem6']) as $m){
        if($m){$members[] = $m;}
      }
      if(!$s['start'] && !$s['stop']) {$status='';}
      if( $s['start'] && !$s['stop']) {
        $status='In Progress';
        $pct = ' - 0%';
        if($s['correctpass']){$pct=' - 33%';}
        if($s['correctval']) {$pct=' - 66%';}
        $status = $status.$pct;
      }
      if($s['start'] &&  $s['stop']) {$status='Finished';}
      if($s['stop']) {
        $tt = strtotime($s['stop']) - strtotime($s['start']);
        $clues = $s['clues'] * 60;
        $cheats = $s['cheat'] * 60 * 5;
        $result=date("i:s", $tt + $clues + $cheats);
      }
                  else {$result ='';}
      $flags = array('correctpass' => $s['correctpass'],
                     'correctval'  => $s['correctval'],
                     'finish'      => $s['finish'],
                     'failval'     => $s['failval'],
                     'cheat'       => $s['cheat'],
                     'sysfail'     => $s['sysfail'],
                     'wrongcmd'    => $s['wrongcmd'],
                     'duration'    => (strtotime($s['stop']) - strtotime($s['start'])));
      $schedule[] = array($s['time'], $s['name'], $members, $status, $result, $flags);
    }

    // BUILD SCHEDULE TABLE
    $out = '';
    $r = $schedule;
    if($_GET['single']){ $cols=2; $len=42; } else { $cols=3; $len=21; }
    for($i=1;$i<$cols;$i++){
      $out .= '<table>';
      $out .= '<tr><th>Time</th><th>Team / Members</th><th>Status</th></tr>';
      if($i==2){$z=21;}else{$z=0;}
      for($x=$z;$x<($len * $i);$x++) {
        $row = '';
        $row .= '<tr>';
          $row .= '<td class="time_td">';
            $row .= date("g:ia", strtotime($r[$x][0]));
          $row .= '</td>';
          $row .= '<td class="team_td">';
            $row .= $r[$x][1];
            $row .= '<br>';
            $counter=1;
            $count=count($r[$x][2]);
            $row .= '<div class="members">';
            foreach($r[$x][2] as $m){
              $row .= $m;
              if($counter < $count) {$row .= ', ';}
              $counter++;
            }
            $row .= '</div>';
          $row .= '</td>';
          // $row .= '<td>';
          // $row .= '</td>';
          $class3 = '';
          if($r[$x][5]['finish']){$class3 = 'green';}
          if($r[$x][5]['sysfail']){$class3 = 'red';}
          if($r[$x][5]['duration'] > $max_time_allowed){$class3 = 'red';}
          $row .= '<td class="'.$class3.' results_td">';
            if($r[$x][3] == 'Finished') {
              $row .= $r[$x][4];
            } else {
              $row .= $r[$x][3];
            }
          $row .= '</td>';
          // $row .= '<td>';
          // $row .= '</td>';
          // $row .= '<td>';
          //   if(!$r[$x][5]['cheat']
          //   && !$r[$x][5]['wrongcmd']
          //   && !$r[$x][5]['failval']
          //   &&  $r[$x][5]['finish']){$row .= '<img src="images/clean.png" class="badges">';}
          //   if($r[$x][5]['cheat']){$row .= '<img src="images/picard_icon.gif" class="badges">';}
          //   if($r[$x][5]['wrongcmd']
          //   || $r[$x][5]['failval']){$row .= '<img src="images/lost.png" class="badges">';}
          // $row .= '</td>';
        $row .= '</tr>';
        $out .= $row;
      }
      $out .= '</table>';
    }

    echo $out;

if(!$_GET['single']){
    // RANKINGS
    $leaders = getLeaders();

    echo '<div class="rankings">';
      echo '<h4>Rankings:</h4>';
      echo '<ol>';
        foreach($leaders as $rec) {
          echo '<li>';
          echo getTeams($rec['team'])[0]['name'];
          echo'</li>';
        }
      echo '</ol>';
    echo '</div>';
  }
    ?>
</body>
</html>
