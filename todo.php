TODO:
  - Registration
    ✓ wrap all text in div w/ frosted background
    ✓ add fkl.escaperoom@gmail.com links
    ✓ add email reminder
    ✓ add reg code to prevent "refresh" double registations
    - mechanism to prevent dupe team names
    ✓ setup new email address > fkl.missionimplausible@gmail.com
    ✓ swap refs to old email with new
    ✓ remove all "escape room" references
  - Game
    ✓ remove login, leave Team ID entry
    ✓ add team id check when logging in
    ✓ move "Easter Egg"
    ✓ add closing video(s)
    ✓ remove "forgot password" from teamID screen
    X 1 penalty for clues
    ✓ rename "commands": to something not real, but within the story
    X accept GET['room']
  - Board
    X sort options (for dual monitors)
    ✓ fix formatting... it's a mess
    ✓ auto-refresh
    - adjust time for cheating and clues
  - Admin
    ✓ reschedule
    ✓ edit teams
    ✓ start clock for teams
    ✓ stop buttons
    ✓ disabled buttons for already started teams
    ✓ fix clock (JS code needs to be in UTC?)
    ✓ delete team
    ✓ if they cheated?
    ✓ move start/stop buttons to Team_admin
    ✓ move clues input to Team_admin
    ✓ create QR code ref for linkk to Team_admin
    ✓ pre-assign room & show on board
  - Team_admin
    ✓ create it
    X add room select
  - Other
    ✓ Move site to GCS_Server
    ✓ DNS edit
