<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="style_board.css">
    <meta name="viewport" content="user-scalable=no, width=device-width">
    <?php
      if(!$_POST['edit_team']
      && !$_POST['move']){
        echo '<meta http-equiv="refresh" content="10">';
      }
    ?>
    <title>Mission:Implausible Admin</title>
  </head>
  <body>
    <img src="qrcode.php?'.$team['id'].'" alt="">
    <?php
    require('config.php');
    require('sql.php');
    require('includes.php');

    // FETCH POST DATA
    if($_POST['delete_team']){
      $id = getSchedule(NULL, false, NULL, $_POST['delete_team'], NULL)[0]['id'];
      update('schedule', $id, 'team', 'NULL', TRUE);
      deleteTeam($_POST['delete_team']);
    }
    if($_POST['single']){
      newRecord($_POST['single'], date("Y-m-d H:i:s"));
    }
    if($_POST['both']){
      $scs = getSchedule(NULL, false, NULL, NULL, $_POST['both']);
      foreach($scs as $sc) {
        newRecord($sc['team'], date("Y-m-d H:i:s"));
      }
    }
    if($_POST['singleStop']) {
      update('records', $_POST['singleStop'], 'stop', date("Y-m-d H:i:s"));
    }
    if($_POST['edit_team']) {
      $team = getTeams($_POST['edit_team'])[0];
      echo '<form method="post">';
      echo 'Editing '.$team['id'].'<br>';
      echo '<label for="name">name</label><br>';
      echo '<input type="text" name="name" id="name" value="'.$team['name'].'"><br>';
      echo '<label for="mem1">mem1</label><br>';
      echo '<input type="text" name="mem1" id="mem1" value="'.$team['mem1'].'"><br>';
      echo '<label for="mem2">mem2</label><br>';
      echo '<input type="text" name="mem2" id="mem2" value="'.$team['mem2'].'"><br>';
      echo '<label for="mem3">mem3</label><br>';
      echo '<input type="text" name="mem3" id="mem3" value="'.$team['mem3'].'"><br>';
      echo '<label for="mem4">mem4</label><br>';
      echo '<input type="text" name="mem4" id="mem4" value="'.$team['mem4'].'"><br>';
      echo '<label for="mem5">mem5</label><br>';
      echo '<input type="text" name="mem5" id="mem5" value="'.$team['mem5'].'"><br>';
      echo '<label for="mem6">mem6</label><br>';
      echo '<input type="text" name="mem6" id="mem6" value="'.$team['mem6'].'"><br>';
      echo '<label for="email">email</label><br>';
      echo '<input type="text" name="email" id="email" value="'.$team['email'].'"><br>';
      echo '<input type="hidden" name="update_team" id="update_team" value="'.$team['id'].'"><br>';
      echo '<button type="submit">Update</button>';
      echo '</form>';
      echo '<form method="post">';
      echo '<button id="delete_team" name="delete_team" value="'.$team['id'].'" onclick="return confirm(\'Are you REALLY sure that you want to DELETE this team?!?!?\');">Delete Team</button>';
      echo '</form>';
      die();
    }
    if($_POST['update_team']){
      update('teams', $_POST['update_team'], 'name', $_POST['name']);
      update('teams', $_POST['update_team'], 'mem1', $_POST['mem1']);
      update('teams', $_POST['update_team'], 'mem2', $_POST['mem2']);
      update('teams', $_POST['update_team'], 'mem3', $_POST['mem3']);
      update('teams', $_POST['update_team'], 'mem4', $_POST['mem4']);
      update('teams', $_POST['update_team'], 'mem5', $_POST['mem5']);
      update('teams', $_POST['update_team'], 'mem6', $_POST['mem6']);
      update('teams', $_POST['update_team'], 'email', $_POST['email']);
    }
    if($_POST['to']){
      $team = $_POST['from'];
      $from_old_sch_id = getSchedule(NULL, false, NULL, $team, NULL)[0]['id'];
      update('schedule', $_POST['to'], 'team', $team, TRUE);
      update('schedule', $from_old_sch_id, 'team', 'NULL', TRUE);
    }
    if($_POST['swap']){
      $a_team =$_POST['swap'];
      $b_team =$_POST['from'];
      $a_old_sched_id = getSchedule(NULL, false, NULL, $_POST['swap'], NULL)[0]['id'];
      $b_old_sched_id = getSchedule(NULL, false, NULL, $_POST['from'], NULL)[0]['id'];
      update('schedule', $a_old_sched_id, 'team', $b_team, TRUE);
      update('schedule', $b_old_sched_id, 'team', $a_team, TRUE);
    }

    // BUILD THE BOARD
    $bd = getBoardData();
    $bd_count = count($bd);

    // BUILD SCHEDULE DATA
    $schedule = array();
    foreach($bd as $s) {
      $members = array();
      foreach(array($s['mem1'],$s['mem2'],$s['mem3'],$s['mem4'],$s['mem5'],$s['mem6']) as $m){
        if($m){$members[] = $m;}
      }
      if(!$s['start'] && !$s['stop']) {$status='';}
      if( $s['start'] && !$s['stop']) {
        $pct = '0%';
        if($s['correctpass']){$pct='33%';}
        if($s['correctval']) {$pct='66%';}
        $status = $pct;
      }
      if($s['start'] &&  $s['stop']) {$status='Finished';}
      if($s['stop']) {
        $tt = strtotime($s['stop']) - strtotime($s['start']);
        $clues = $s['clues'];
        $cheats = $s['cheat'];
        $result = date("i:s", $tt);
        $result = "$result<br><span class='smaller'>Clues: $clues - Cheats: $cheats </span>";
      } else {
        $result ='';
      }

      $flags = array('correctpass' => $s['correctpass'],
                     'correctval'  => $s['correctval'],
                     'finish'      => $s['finish'],
                     'failval'     => $s['failval'],
                     'egg'         => $s['egg'],
                     'cheat'       => $s['cheat'],
                     'clues'       => $s['clues'],
                     'sysfail'     => $s['sysfail'],
                     'wrongcmd'    => $s['wrongcmd'],
                     'teamnum'     => $s['team'],
                     'start'       => $s['start'],
                     'stop'        => $s['stop'],
                     'id'          => $s['id'],
                     'duration'    => (strtotime($s['stop']) - strtotime($s['start'])));
      $schedule[] = array($s['time'], $s['name'], $members, $status, $result, $flags);
    }

    // BUILD SCHEDULE TABLE
    $out = '';
    $r = $schedule;
    $out .= '<table class="admintable">';
    $out .= '<tr><th></th><th>Time</th><th>Team#</th><th>Team / Members</th><th>Status</th><th>Badges</th></tr>';
    for($x=0;$x<42;$x++) {
      $row = '';
      $row .= '<tr>';
        // if($x % 2 == 0){
        //   if(!$r[$x][5]['start']
        //   && !$r[($x +1)][5]['start']){
        //     $row .= '<td rowspan="2" class="db_td">';
        //       $row .= '<form method="post">';
        //       $row .= '<button class="dualBTN" name="both" value="'.$r[$x][0].'">Start<br>Both</button>';
        //       $row .= '</form>';
        //     $row .= '</td>';
        //   } else {
        //     $row .= '<td rowspan="2" class="db_td">';
        //     $row .= '</td>';
        //   }
        // }
        // $row .= '<td class="sb_td">';
        //   if(!$r[$x][5]['start']){
        //     $row .= '<form method="post">';
        //     $row .= '<button class="singleBTN" name="single" value="'.$r[$x][5]['teamnum'].'">Start</button>';
        //     $row .= '</form>';
        //   } elseif(!$r[$x][5]['stop']) {
        //     $row .= '<form method="post">';
        //     $row .= '<button class="singleBTNstop" name="singleStop" value="'.$r[$x][5]['teamnum'].'" onclick="return confirm(\'Are you sure you want to stop?\')">Stop</button>';
        //     $row .= '</form>';
        //   }
        // $row .= '</td>';
        if($r[$x][1]){
          $row .= '<td class="qb_td">';
          $row .= '<a href="team_admin.php?team='.$r[$x][5]['teamnum'].'">';
          $row .= '<img src="images/edit.png" width="30">';
          $row .= '</a>';
          $row .= '</td>';
        } else {
          $row .= '<td>';
          $row .= '</td>';
        }
        $row .= '<td class="time_td">';
          $row .= date("g:ia", strtotime($r[$x][0]));
        $row .= '</td>';
        $row .= '<td class="teamnum_td">';
          if($_POST['move']){
            if($_POST['move'] == $r[$x][5]['teamnum']){
              $row .= '<form method="post">
                        <button class="scheduleBTNs cancelBTN" type="submit" name="cancel">Cancel</button>
                       </form>';
            } elseif($r[$x][5]['teamnum']) {
              $row .= '<form method="post">
                        <button class="scheduleBTNs swapBTN" type="submit" name="swap" value="'.$r[$x][5]['teamnum'].'">Swap</button>
                        <input type="hidden" name="from" value="'.$_POST['move'].'">
                       </form>';
            } else {
              $row .= '<form method="post">
                        <button class="scheduleBTNs moveBTN" type="submit" name="to" value="'.$r[$x][5]['id'].'">Move</button>
                        <input type="hidden" name="from" value="'.$_POST['move'].'">
                       </form>';
            }
          } elseif($r[$x][5]['teamnum']) {
            $row .= $r[$x][5]['teamnum'];
            $row .= '&nbsp;';
            $row .= '<form method="post"><button class="scheduleBTNs updnBTN" type="submit" name="move" value="'.$r[$x][5]['teamnum'].'">⇵</button></form>';
          }

        $row .= '</td>';
        $row .= '<td class="team_td">';
          $row .= $r[$x][1];
          $row .= '<br>';
          $counter=1;
          $count=count($r[$x][2]);
          $row .= '<div class="members">';
          foreach($r[$x][2] as $m){
            $row .= $m;
            if($counter < $count) {$row .= ', ';}
            $counter++;
          }
          if($r[$x][5]['teamnum']){
            $row .= '</div>';
            $row .= '<form method="post">';
            $row .= '<span class="editTeam"><button type="submit" name="edit_team" value="'.$r[$x][5]['teamnum'].'">edit</button>';
            $row .= '</form">';
          }
        $row .= '</td>';
        $class3 = '';
        if($r[$x][5]['finish']){$class3 = 'green';}
        if($r[$x][5]['sysfail']){$class3 = 'red';}
        if($r[$x][5]['duration'] > $max_time_allowed){$class3 = 'red';}
        $row .= '<td class="'.$class3.' results_td">';
          if($r[$x][3] == 'Finished') {
            $row .= $r[$x][4];
          } elseif($r[$x][3] != '') {
            $datetime = date("H:i:s", strtotime($r[$x][5]['start']));
            $nowtime =  date("H:i:s");
            $difftime = date("i:s", (strtotime($nowtime) - strtotime($datetime)));
            $row .= '<div class="clock" id="'.$x.'clock'.'">'.$difftime.'</div><br>';
            $row .= '<div class="progress">'.$r[$x][3].'</div>';
            $row .= '<script>
                     var countDownDate = new Date("'.$r[$x][5]['start'].' GMT+00:00").getTime();
                     var x = setInterval(function() {
                       var now = new Date().getTime();
                       var distance = now - countDownDate;
                       var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                       var seconds = Math.floor((distance % (1000 * 60)) / 1000);
                       document.getElementById("'.$x.'clock'.'").innerHTML = minutes + ":" + pad(seconds);
                     }, 1000);
                     </script>';
          }
        $row .= '</td>';
        $row .= '<td class="badges_td">';
          if(!$r[$x][5]['egg']
          && !$r[$x][5]['clues']
          && !$r[$x][5]['cheat']
          && !$r[$x][5]['wrongcmd']
          && !$r[$x][5]['failval']
          &&  $r[$x][5]['finish']){$row .= '<img src="images/clean.png" class="badges">';}
          if($r[$x][5]['egg'])    {$row .= '<img src="images/picard_icon.gif" class="badges">';}
          if($r[$x][5]['cheat'])  {$row .= '<img src="images/no.png" class="badges">';}
          if($r[$x][5]['wrongcmd']
          || $r[$x][5]['failval']
          || $r[$x][5]['clues'])  {$row .= '<img src="images/lost.png" class="badges">';}
        $row .= '</td>';
      $row .= '</tr>';
      $out .= $row;
    }
    $out .= '</table>';

    echo $out;

    ?>
</body>
</html>
<script>
function pad(num) {
  var s = num+"";
  while (s.length < 2) s = "0" + s;
  return s;
}
</script>
